#!/usr/bin/env python
# coding:utf-8

from toughwifi.common import utils
from toughwifi.console.handlers.base import BaseHandler, MenuSys
from toughwifi.common.permit import permit
from toughwifi.console import models
from toughwifi.console.handlers import password_forms
from cyclone.web import authenticated
from hashlib import md5


@permit.route(r"/wifidog/ping", u"wifidog ping", MenuSys, order=9.0000)
class WifidogPingHandler(BaseHandler):

    def get(self):
        print self.request.query
        self.write("pong")


@permit.route(r"/wifidog/auth", u"wifidog auth", MenuSys, order=9.0001)
class WifidogAuthHandler(BaseHandler):

    def get(self):
        print self.request.query
        token = self.get_argument("token")

        if token in ("test"):
            self.write("Auth: 1")
        else:
            self.write("Auth: 0")


@permit.route(r"/wifidog/login", u"wifidog login", MenuSys, order=9.0002)
class WifidogLoginHandler(BaseHandler):

    def get(self):
        print self.request.query
        gw_port = self.get_argument("gw_port",0)
        gw_address = self.get_argument("gw_address",0)
        print gw_address, gw_port
        self.write("login")

@permit.route(r"/wifidog/portal", u"wifidog portal", MenuSys, order=9.0003)
class WifidogPortalHandler(BaseHandler):

    def get(self):
        print self.request.query
        self.write("portal")