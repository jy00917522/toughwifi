#!/usr/bin/env python
# coding:utf-8
import cyclone.auth
import cyclone.escape
import cyclone.web
import base
from toughwifi.console.forms import config_forms
from toughwifi.console.handlers.base import MenuSys

from toughwifi.common.permit import permit
from cyclone.web import authenticated
from toughwifi.console import models
import ConfigParser

@permit.route(r"/config", u"参数配置管理", MenuSys, order=1.0011, is_menu=True)
class ConfigHandler(base.BaseHandler):
    @authenticated
    def get(self):
        active = self.get_argument("active", "default")
        default_form = config_forms.default_form()
        default_form.fill(self.settings.config.defaults)
        database_form = config_forms.database_form()
        database_form.fill(self.settings.config.database)


        self.render("config.html",
                    active=active,
                    default_form=default_form,
                    database_form=database_form
                    )

@permit.route(r"/config/default/update", u"默认配置", MenuSys, order=1.0022)
class DefaultHandler(base.BaseHandler):
    @cyclone.web.authenticated
    def post(self):
        config = self.settings.config
        config.defaults.debug = self.get_argument("debug")
        config.defaults.tz = self.get_argument("tz")
        config.defaults.syslog_enable = self.get_argument("syslog_enable")
        config.defaults.syslog_server = self.get_argument("syslog_server")
        config.defaults.syslog_port = self.get_argument("syslog_port")
        config.update()
        self.redirect("/config?active=default")


@permit.route(r"/config/database/update", u"数据库配置", MenuSys, order=1.0023)
class DatabaseHandler(base.BaseHandler):
    @cyclone.web.authenticated
    def post(self):
        config = self.settings.config
        config.database.echo = self.get_argument("echo")
        config.database.dbtype = self.get_argument("dbtype")
        config.database.dburl = self.get_argument("dburl")
        config.database.pool_size = self.get_argument("pool_size")
        config.database.pool_recycle = self.get_argument("pool_recycle")
        config.database.backup_path = self.get_argument("backup_path")
        config.update()

        self.redirect("/config?active=database")

